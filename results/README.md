
#### Hyper-parameters ####
- Train samples - 900
- Test samples - 100
- Learning rate - 0.003
- Hidden units -  50
- Epochs - 50, 100
- Optimizer - Gradient Descent
- Update Type - Sequence

![alt text](birnn_with_fc.jpg)
![alt text](birnn_with_fc10.jpg)
![alt text](single_rnn.jpg)
![alt text](reverse_rnn.jpg)

#### Observations ####

- Tanh + Sigmoid works better compared to other activation functions.
- The concatenation size of 2 (1 from each RNN) works best. We experimented on concatenation sizes of 5, 10 and 20.
- Bptt truncation of 1 gives more accuracy than other window sizes.
- Bi-RNN with FC layers in 50 epochs achieves more accuracy than single RNN with 100 epochs.
- With Bi-RNN the accuracy decreases when the number of epochs is increased. 50 epochs gives better accuracy than 100 epochs.
- The experiments are all conducted with end of sequence updates in order to compare with bi-RNN
