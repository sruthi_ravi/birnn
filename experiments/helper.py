# import the right model file in the helper
from core.bi_rnn_concat import BiRNN as Model
from core.activation import Softmax
from core.activation import Sigmoid
from core.activation import SigmoidwithCrossEntropy
import numpy as np

parameter_file = {
    'u': '../parameters/u.npy',
    'v': '../parameters/v.npy',
    'w': '../parameters/w.npy',
    'u_bi': '../parameters/u_bi.npy',
    'w_bi': '../parameters/w_bi.npy',
    'fc_w': '../parameters/fc_w.npy',
    'fc_b': '../parameters/fc_b.npy'
}
data_path = {
    'x_train': '../data/gen/100_train_X.npy',
    'y_train': '../data/gen/100_train_Y.npy',
    'x_test': '../data/gen/100_test_X.npy',
    'y_test': '../data/gen/100_test_Y.npy'
}


# Generate data for sequence classification for numbers generated randomly within 10.
def get_data(model=None):
    np.random.seed(seed=1)
    # Create dataset
    nb_of_samples = 1000
    sequence_len = 10
    # Create the sequences
    x = np.random.randint(10, size=(nb_of_samples, sequence_len))
    y = np.zeros((nb_of_samples, sequence_len), dtype=int)
    limit = sequence_len * 10 / 4
    for i in range(len(x)):
        a = np.cumsum(x[i])
        y[i] = np.array([0 if el < limit else 1 for el in a], dtype=int)
    if model == 'keras':
        x = x.reshape(nb_of_samples, 1, sequence_len, 1)
        y = y.reshape(nb_of_samples, 1, sequence_len, 1)
    x_train = x[:900]
    x_test = x[900:]
    y_train = y[:900]
    y_test = y[900:]
    # write_to_file(x_train, y_train, x_test, y_test)
    return x_train, y_train, x_test, y_test


# Generate data for sequence summing to 100.
def get_norm_data(model=None):
    np.random.seed(seed=1)
    # Create dataset
    nb_of_samples = 1000
    sequence_len = 10
    # Create the sequences
    x = np.random.dirichlet(np.ones(sequence_len), size=nb_of_samples)
    ### start
    # for sequence summing 100
    x = np.around(x, decimals=2)
    x *= 100
    sum = np.sum(x, axis=1)
    diff = 100 - sum
    last_col = (x[:, -1] + diff).reshape(nb_of_samples, 1)
    last_col[last_col < 0] = 0
    x = np.concatenate((x[:, :-1], last_col), axis=1)
    x = x.astype(int)

    ### end
    y = np.zeros((nb_of_samples, sequence_len), dtype=int)
    limit = round(sequence_len * 10 / 3)
    for i in range(len(x)):
        a = np.cumsum(x[i])
        y[i] = np.array([0 if el < limit else 1 for el in a], dtype=int)
    # Keras model needs data to be normalized in the below manner as 3 dimension.
    if model == 'keras':
        x = x.reshape(nb_of_samples, 1, sequence_len, 1)
        y = y.reshape(nb_of_samples, 1, sequence_len, 1)
    # reverse the sequence
    # x = x[:, ::-1]
    # y = y[:, ::-1]
    x_train = x[:900]
    x_test = x[900:]
    y_train = y[:900]
    y_test = y[900:]
    # write_to_file(x_train, y_train, x_test, y_test)
    return x_train, y_train, x_test, y_test


def float_vector(z, size):
    bits = np.float16(z).view(np.int16)
    binary = '{:016b}'.format(bits)
    return np.array([int(val) for val in binary])


def word_vector(z, size):
    binary = '{0:07b}'.format(z)
    return np.array([int(val) for val in binary])


def write_output(file, data):
    with open(file, 'w') as fw:
        for val in data:
            fw.write(','.join(str(v) for v in val) + '\n')
    fw.close()


def write_to_file(x_train, y_train, x_test, y_test):
    # np.save(data_path['x_train'], x_train)
    # np.save(data_path['y_train'], y_train)
    # np.save(data_path['x_test'], x_test)
    # np.save(data_path['y_test'], y_test)
    write_output(data_path['x_train'], x_train)
    write_output(data_path['y_train'], y_train)
    write_output(data_path['x_test'], x_test)
    write_output(data_path['y_test'], y_test)


def write_weights(array, layer):
    """
    Accepts a numpy array consisting of weight matrix and writes the weights to a file
    :param array:
    :return: None
    """
    row_dim, col_dim = array.shape
    filename = 'v_interim_hid_out'
    if layer == 'input':
        filename = 'u_interim_in_hid'
    elif layer == 'hidden':
        filename = 'w_interim_hid_hid'
    with open('../debug/' + filename, 'w') as f:
        for row in range(row_dim):
            for col in range(col_dim):
                f.write(str(col) + ' ' + str(row) + ' ' + str(array[row][col]) + '\n')
    f.close()


def write_weight_bit(weight_map, input):
    # order is U,W,V
    with open('../debug/uvw', 'a') as f:
        f.write('\nInput element {}'.format(input))
        for key, ele in weight_map.items():
            f.write('\nWeight {}\n {}\n'.format(key, ele))
    f.close()


def one_training(train_x, rnn):
    output = SigmoidwithCrossEntropy()
    layers = rnn.forward_propagation(train_x)
    for layer in layers:
        print('------------------------------------------------------------')
        print('Final prediction after sigmoid \n {} \n'.format(output.predict(layer.mulv)))
        print('Hidden to Hidden \n{}\n'.format(layer.s))
        print('Add gate values \n{}\n'.format(layer.add))
        print('Mulw values \n{}\n'.format(layer.mulw))
        print('Mulu values \n{}\n'.format(layer.mulu))
        print('------------------------------------------------------------')


def init_model(u=None, v=None, w=None, u_bi=None, w_bi=None, fc_w=None, fc_b=None):
    word_dim = 7  # generate binarized values of numbers from 0-100 (2^7)
    hidden_dim = 50
    output_dim = 1
    return Model(word_dim, output_dim, Sigmoid, word_vector, hidden_dim=hidden_dim, bptt_truncate=0,
                 learning_rate=0.005, optimizer=None,
                 update_type='sequence', u=u, v=v, w=w, u_bi=u_bi, w_bi=w_bi, fc_w=fc_w, fc_b=fc_b)
