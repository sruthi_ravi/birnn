from random import random
import numpy as np
import timeit
from matplotlib import pyplot
from pandas import DataFrame
from keras.models import Sequential
from keras.layers import LSTM
from keras.layers import SimpleRNN
from keras.layers import Dense
from keras.layers import TimeDistributed
from keras.layers import Bidirectional
from experiments import helper


# create a sequence classification instance
def get_sequence(n_timesteps):
    # create a sequence of random numbers in [0,1]
    X = np.array([random() for _ in range(n_timesteps)])
    # calculate cut-off value to change class values
    limit = n_timesteps / 4.0
    # determine the class outcome for each item in cumulative sequence
    y = np.array([0 if x < limit else 1 for x in np.cumsum(X)])
    # reshape input and output data to be suitable for LSTMs
    X = X.reshape(1, n_timesteps, 1)
    y = y.reshape(1, n_timesteps, 1)
    return X, y


def get_lstm_model(n_timesteps, backwards):
    model = Sequential()
    model.add(LSTM(20, input_shape=(n_timesteps, 1), return_sequences=True, go_backwards=backwards))
    model.add(TimeDistributed(Dense(1, activation='sigmoid')))
    model.compile(loss='binary_crossentropy', optimizer='adam')
    return model


def get_bi_lstm_model(n_timesteps, mode):
    model = Sequential()
    model.add(Bidirectional(SimpleRNN(10, return_sequences=True), input_shape=(n_timesteps, 1), merge_mode=mode))
    model.add(TimeDistributed(Dense(1, activation='sigmoid')))
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    return model


def train_model(model, X, Y):
    loss = list()
    for eph in range(100):
        for i in range(len(X)):
            # fit model for one epoch on this sequence
            hist = model.fit(X[i], Y[i], epochs=1, batch_size=1, verbose=0)
            loss.append(hist.history['loss'][0])
        print('Loss after {} epoch is:{}'.format(eph, loss[-1]))
    return loss


def main():
    results = DataFrame()
    x_train, y_train, x_test, y_test = helper.get_norm_data(model='keras')
    n_timesteps = 10
    model = get_bi_lstm_model(n_timesteps, 'concat')
    results['bilstm_con'] = train_model(model, x_train, y_train)
    score = model.evaluate(x_test[0], y_test[0], batch_size=1, verbose=1)
    print('Test score:', score[0])
    print('Test accuracy:', score[1])

    model_json = model.to_json()
    with open("../parameters/keras_model.json", "w") as json_file:
        json_file.write(model_json)
    # serialize weights to HDF5
    model.save_weights("../parameters/keras_model.h5")
    print("Saved model to disk")


if __name__ == '__main__':
    exec_time = '{:.2f}s'.format(timeit.timeit("main()", setup="from classify_seq_keras_train import main", number=1))
    print(exec_time)
