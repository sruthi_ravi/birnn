import numpy as np
from experiments import helper


def predict(x_test, y_test):
    # read the parameters from the numpy file and do the forward propagation
    u = np.load(helper.parameter_file['u'])
    v = np.load(helper.parameter_file['v'])
    w = np.load(helper.parameter_file['w'])
    u_bi = np.load(helper.parameter_file['u_bi'])
    w_bi = np.load(helper.parameter_file['w_bi'])
    fc_b = np.load(helper.parameter_file['fc_b'])
    fc_w = np.load(helper.parameter_file['fc_w'])
    rnn = helper.init_model(u=u, v=v, w=w, u_bi=u_bi, w_bi=w_bi, fc_w=fc_w, fc_b=fc_b)
    accurate = 0
    y_predict = np.zeros(x_test.shape)
    with open('../output/classify_birnn.txt', 'w') as fw:
        for i in range(len(x_test)):
            unround_output = rnn.predict(x_test[i])
            output = np.round(unround_output)
            np.append(y_predict, output)
            if np.array_equal(y_test[i], output):
                accurate += 1
            fw.write(str(output) + '\n')
        fw.write(
            'accuracy percentage based on number of correct predictions is {}'.format(accurate * 100 / len(x_test)))

    print('accuracy percentage based on number of correct predictions is {}'.format(accurate * 100 / len(x_test)))


def main():
    x_test = np.load(helper.data_path['x_test'])
    y_test = np.load(helper.data_path['y_test'])
    print('Testing....')
    predict(x_test, y_test)


if __name__ == '__main__':
    main()
