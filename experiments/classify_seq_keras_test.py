# load json and create model
from keras.models import model_from_json
from experiments import helper
import numpy as np
x_train, y_train, x_test, y_test = helper.get_norm_data(model='keras')

json_file = open('../parameters/keras_model.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)
# load weights into new model
loaded_model.load_weights("../parameters/keras_model.h5")
print("Loaded model from disk")

# evaluate loaded model on test data
loaded_model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
for layer in loaded_model.layers:
    config=layer.get_config()
    weights=layer.get_weights()
    print(config)
    print(len(weights))
accurate = 0
for i in range(100):
    score = loaded_model.evaluate(x_test[i], y_test[i], verbose=0)
    yhat = loaded_model.predict_classes(x_test[i], verbose=0).reshape(1, 10)
    if np.array_equal(y_test[i].reshape(1,10), yhat):
        accurate += 1
print('accuracy percentage based on number of correct predictions is {}'.format(accurate * 100 / len(x_test)))

print("%s: %.2f%%" % (loaded_model.metrics_names[1], score[1] * 100))
print(loaded_model.get_layer(name="bidirectional_1").input_shape)
print(loaded_model.get_layer(name="bidirectional_1").output_shape)
# print(loaded_model.get_layer(name="lstm_1").input_shape)
# print(loaded_model.get_layer(name="lstm_1").output_shape)
print(loaded_model.get_layer(name="time_distributed_1").input_shape)
print(loaded_model.get_layer(name="time_distributed_1").output_shape)
# print(loaded_model.get_layer(name="dense_1").input_shape)
# print(loaded_model.get_layer(name="dense_1").output_shape)