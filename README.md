# README #

### Sequence classification using Birectional RNN
This repository has a working model of bidirectional RNN architecture experimented on sequence classification. The network is trained using randomly generated sequence dataset. The architecture consists of bi-directional RNN layers and FC layer. 
At each time instance, a number is fed as input to the RNN and 0 or 1 is predicted as output depending on the cumulative sum of the sequence.
During the test phase, the network predicts the binary output for each number in the sequence.
We also experimented on vannila bi-RNN model without FC layer and bi-RNN model with FC layer. The bi-RNN model with FC layer works best for this dataset. For sanity check we also configured and ran the keras model for this dataset.

### DataSet ###

* The dataset contains sequence of length 10 of randomly generated numbers. The output is a binary label (0 or 1) is associated with each input. Once the cumulative sum of the input values in the sequence exceeds a threshold, then the output value flips from 0 to 1. 
Threshold is the cumulative sum of the numbers in the sequence. For a sequence length of 10, the maximum cumulative sum is 100 where the numbers are of the range (0,10). In this case, the threshold 1/4 of 100 = 25
The sequence length and threshold is configurable and the data can be generated again with different values.

* Train/Test split
Train data - 900 samples
Test data - 100 samples

#### Data Sample ####
* /data/gen/100_[train/test]
    Generate sequence of length 10 that sums upto 100. Each number in the range (0,100)
    - Sample X: 12,28,0,8,4,2,5,10,12,18
    - Sample Y: 0,1,1,1,1,1,1,1,1,1
    The output bit (Y) changes from 0 to 1 when the cumulative sum of sequence exceeds the threshold value. 

* /data/gen/100_rev_[train/test]
    Reverse sequence of length 10 that sums upto 100. Each number in the range (0,100). The threshold here is 100 - threshold of the forward sequence.
    - Sample X: 18,12,10,5,2,4,8,0,28,12
    - Sample Y: 0,0,0,0,0,0,0,0,1,1
     The output bit (Y) changes from 0 to 1 when the cumulative sum of sequence exceeds the threshold value. 

### Architecture ###
The bi-RNN is created using two single RNNs in forward and reverse direction. The output of the hidden layer of both the RNNs are concatenated before the final output RNN (V) layer. The output of combined RNN is passed to the single FC layer. 
.The intermediate layers use Tanh activation function and final output layer uses Sigmoid for binary classification.

### How do I set up? ###

* Model set up
    - /core contains the files for the bi-RNN model architecture.
    - /core/activation - Forward and backward logic for activation functions 
    - /core/gates - RNN gate operations
    - /core/layer - Layer architecture
    - /core/bi_rnn_concat - Bi-RNN Model architecture
* Experiment set up
    /experiments contains the train and test set up files
    /experiments/helper - Helper files for model initialization and configuration.
    - init_model function - defines the configuration such as input, hidden and output dimension and the hyper-parameters for bi-RNN.
    - The helper file also defines the location of the data and parameters weights to save the weights after training. The same paths are used during test time.  
    - /experiments/classify_seq_bitrain - Loads the train data and invoke the model training.
    - /experiments/classify_seq_bitest - Loads the test data and invoke the model prediction.
    - /experiments/classify_seq_keras_train - Use the classification datatset and configure the keras bi-RNN network training.
    - /experiments/classify_seq_keras_test - Use the classification datatset and configure the keras bi-RNN network testing.
      The keras model weights are saved in model.json file and model.h5 format under /parameters.
     
* Output and Parameters
    - /output - saves the predicted Y values and accuracy in this directory under classify_birnn.txt file.
    - /parameters - saves the model parameters weight matrices files in this directory
    parameter_file = {
    'u': '../parameters/u.npy', # forward RNN input to hidden weight
    'w': '../parameters/w.npy', # # forward RNN hidden to hidden weight
    'u_bi': '../parameters/u_bi.npy', # reverse RNN input to hidden weight
    'w_bi': '../parameters/w_bi.npy', # reverse RNN hidden to hidden weight
    'v': '../parameters/v.npy', # concatenated weight of 2 RNNs hidden to output weight
    'fc_w': '../parameters/fc_w.npy', # FC layer weight
    'fc_b': '../parameters/fc_b.npy', # FC layer bias
}
    - For the keras model, the parameters are saved in 
        - /parameters/keras_model.h5
        - /parameters/keras_model.json
    This path is used to save and read the model in train and test files.
    
    
* Configurable parameters
    * Epochs
    * Learning rate
    * RNN Input, hidden and output dimension
    * bptt truncate size
    * Optimizer - 'adam' or 'gradient descent'. If nothing is specified default is gradient descent.
    * Parameter update type - 'sequence' or 'window' for end of sequence or window updates.
    * Output activation function 
    * Bi-RNN concatenation size
    * FC layer input, hidden and output dimension
    * Tanh + Softmax works better 
    * Generator function to normalize the input fed int RNN at every time instance defined in /experiments/helper
    

* How to run
    - To start training, run experiments/classify_seq_bitrain file.
    - To start testing, run the /experiments/classify_seq_bitest file. Check for the correct saved parameters path in /experiments/helper file.
    - To start training keras model, run experiments/classify_seq_keras_train file.
    - To start testing, run the /experiments/classify_seq_keras_test file.