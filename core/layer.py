from core.activation import Tanh
from core.activation import Sigmoid
from core.gates import AddGate, MultiplyGate
import numpy as np

mulGate = MultiplyGate()
addGate = AddGate()
activation = Tanh()


class RNNLayer:
    def forward(self, x, prev_s, U, W, V):
        self.mulu = mulGate.forward(U, x)
        self.mulw = mulGate.forward(W, prev_s)
        self.add = addGate.forward(self.mulw, self.mulu)
        self.s = activation.forward(self.add)
        self.mulv = mulGate.forward(V, self.s)

    def backward(self, x, prev_s, U, W, V, diff_s, dmulv):
        # self.forward(x, prev_s, U, W, V)
        # predict = output.predict(self.mulv)
        # dmulv = output.derivative(self.mulv, y)
        dV, dsv = mulGate.backward(V, self.s, dmulv)
        ds = dsv + diff_s
        dadd = activation.backward(self.add, ds)
        dmulw, dmulu = addGate.backward(self.mulw, self.mulu, dadd)
        dW, dprev_s = mulGate.backward(W, prev_s, dmulw)
        dU, dx = mulGate.backward(U, x, dmulu)
        # with open('/Users/sruthiravi/DR/sigma-rnn/rnnexp/sigma/weights/backward', 'a') as f:
        #     f.write('---------------------------------------------')
        #     f.write('\nComputations inside backward for input: {}\n'.format(x))
        #     # f.write('Predicted final mulv after activation: {}\n'.format(predict))
        #     f.write('dmulv after forward propagation dmulv\n {}\n'.format(dmulv))
        #     f.write('Differtial of V - dV\n{}\n'.format(dV))
        #     f.write('Differtial of s - ds\n{}\n'.format(ds))
        #     f.write('Differtial of add - dadd\n{}\n'.format(dadd))
        #     f.write('Differtial of mulw - dmulw\n{}\n'.format(dmulw))
        #     f.write('Differtial of mulu - dmulu\n{}\n'.format(dmulu))
        #     f.write('Differtial of W- dW\n{}\n'.format(dW))
        #     f.write('Differtial of prev_s - dprev_s\n{}\n'.format(dprev_s))
        #     f.write('Differtial of U - dU\n{}\n'.format(dU))
        #     f.write('Differtial of x - dx\n{}\n'.format(dx))
        #     f.write('End of backward\n')
        #     f.write('---------------------------------------------')
        # f.close()
        return dprev_s, dU, dW, dV


class BiRNNLayer:
    # remove V its not used
    def forward(self, x, prev_s, U, W, V):
        self.mulu = mulGate.forward(U, x)
        self.mulw = mulGate.forward(W, prev_s)
        self.add = addGate.forward(self.mulw, self.mulu)
        self.s = activation.forward(self.add)

    def backward(self, x, prev_s, U, W, dsv, diff_s, dV):
        ds = dsv + diff_s
        dadd = activation.backward(self.add, ds)
        dmulw, dmulu = addGate.backward(self.mulw, self.mulu, dadd)
        dW, dprev_s = mulGate.backward(W, prev_s, dmulw)
        dU, dx = mulGate.backward(U, x, dmulu)
        return dprev_s, dU, dW, dV


class fc(object):
    def __init__(self, input_dim, output_dim, init_scale=0.02, name="fc", fc_w=None, fc_b = None):
        """
        In forward pass, please use self.params for the weights and biases for this layer
        In backward pass, store the computed gradients to self.grads
        - name: the name of current layer
        - input_dim: input dimension
        - output_dim: output dimension
        - meta: to store the forward pass activations for computing backpropagation
        """
        self.name = name
        self.w_name = name + "_w"
        self.b_name = name + "_b"
        self.input_dim = input_dim
        self.output_dim = output_dim
        self.params = {}
        self.grads = {}
        self.params[self.w_name] = fc_w if fc_w is not None else init_scale * np.random.randn(input_dim, output_dim)
        self.params[self.b_name] = fc_b if fc_b is not None else np.zeros(output_dim)
        self.grads[self.w_name] = None
        self.grads[self.b_name] = None
        self.meta = None

    def forward(self, feat):
        """ Some comments """
        output = None
        assert np.prod(feat.shape[1:]) == self.input_dim, "But got {} and {}".format(
            np.prod(feat.shape[1:]), self.input_dim)
        batch_size = len(feat)
        X = feat.flatten().reshape(batch_size, np.prod(self.input_dim))
        output = np.dot(X, self.params[self.w_name]) + self.params[self.b_name]
        self.meta = feat
        return output

    def backward(self, dprev):
        """ Some comments """
        feat = self.meta
        if feat is None:
            raise ValueError("No forward function called before for this module!")
        dfeat, self.grads[self.w_name], self.grads[self.b_name] = None, None, None

        batch_size = len(feat)
        X = feat.flatten().reshape(batch_size, np.prod(self.input_dim))
        self.grads[self.w_name] = np.dot(np.transpose(X), dprev)
        self.grads[self.b_name] = np.sum(dprev, axis=0)
        dfeat = np.dot(dprev, np.transpose(self.params[self.w_name]))
        dfeat = dfeat.flatten().reshape(feat.shape)

        self.meta = None
        return dfeat