import numpy as np


class MultiplyGate:
    def forward(self, W, x):
        return np.dot(W, x)

    def backward(self, W, x, dz):
        dW = np.asarray(np.dot(np.transpose(np.asmatrix(dz)), np.asmatrix(x)))
        dx = np.dot(np.transpose(W), dz)
        return dW, dx


class ConcatGate:
    def forward(self, s, srev):
        return np.concatenate((s, srev), axis=0)

    def backward(self, s, srev, dscon):
        ds = dscon[:len(s)] * np.ones_like(s)
        dsrev = dscon[len(s):] * np.ones_like(srev)
        return ds, dsrev


class AddGate:
    def forward(self, x1, x2):
        return x1 + x2

    def backward(self, x1, x2, dz):
        dx1 = dz * np.ones_like(x1)
        dx2 = dz * np.ones_like(x2)
        return dx1, dx2
